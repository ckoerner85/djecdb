#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Load DJEC output files per cohort:

ex.norm.COADREAD <- readRDS("COADREAD_ex.norm.rds")
Cancer_splicing.norm.COADREAD <-readRDS("COADREAD_Cancer_splicing.norm_relaxfilter.rds")
COADREAD_SigTopTable.norm.plot_updated <- readRDS("COADREAD_SigTopTable.norm.plot_updated.rds")

ex.norm.LUAD <- readRDS("LUAD_ex.norm.rds")
Cancer_splicing.norm.LUAD <-readRDS("LUAD_Cancer_splicing.norm_relaxfilter.rds")
LUAD_SigTopTable.norm.plot_updated <- readRDS("LUAD_SigTopTable.norm.plot_updated.rds")

ex.norm.LUSC <- readRDS("LUSC_ex.norm.rds")
Cancer_splicing.norm.LUSC <-readRDS("LUSC_Cancer_splicing.norm_relaxfilter.rds")
LUSC_SigTopTable.norm.plot_updated <- readRDS("LUSC_SigTopTable.norm.plot_updated.rds")

ex.norm.BRCA <- readRDS("BRCA_ex.norm.rds")
Cancer_splicing.norm.BRCA <-readRDS("BRCA_Cancer_splicing.norm_relaxfilter.rds")
BRCA_SigTopTable.norm.plot_updated <- readRDS("BRCA_SigTopTable.norm.plot_updated.rds")


ex.norm.LGG <- readRDS("LGG_ex.norm.rds")
Cancer_splicing.norm.LGG <-readRDS("LGG_Cancer_splicing.norm_relaxfilter.rds")
LGG_SigTopTable.norm.plot_updated <- readRDS("LGG_SigTopTable.norm.plot.rds")

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Load the annotation file for gene model graphics
GRCh37_gtf <- read.delim("GRCh37_latest_genomic.gtf", header=FALSE, comment.char="#")
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Load matrix eQTL outputs
# mutations
COADREAD_matrixeqtl.mut <- read.delim("mut.data.COADREAD.2.txt", sep = "\t")
LUAD_matrixeqtl.mut <- read.delim("mut.data.LUAD.2.txt", sep = "\t")
LUSC_matrixeqtl.mut <- read.delim("mut.data.LUSC.2.txt", sep = "\t")
BRCA_matrixeqtl.mut <- read.delim("mut.data.BRCA.2.txt", sep = "\t")

# junctions
COADREAD_matrixeqtl.junc <- read.delim("voom.COADREAD.tumor.3.txt", sep = "\t")
LUAD_matrixeqtl.junc <- read.delim("voom.LUAD.tumor.3.txt", sep = "\t")
LUSC_matrixeqtl.junc <- read.delim("voom.LUSC.tumor.3.txt", sep = "\t")
BRCA_matrixeqtl.junc <- read.delim("voom.BRCA.tumor.3.txt", sep = "\t")

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Load JCNA Cytoscape-based outputs
COADREAD_CytoscapeInput <- readRDS("COADREAD_CytoscapeInput_mod_1.rds")
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Load exon-domain information
domain.to.genome <- read.csv("domain.to.genome.csv", sep=";")
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# Define functions:

# interactive plotSplice function
plotSplice.mod <- function (fit, coef = ncol(fit), geneid = NULL, genecolname = NULL, 
                            diffSout = NULL, SigTopTable=NULL, rank = 1L, FDR = 0.01) 
{
  if (is.null(genecolname)) 
    genecolname <- fit$genecolname
  else genecolname <- as.character(genecolname)
  if (is.null(geneid)) {
    if (rank == 1L) 
      i <- which.min(fit$gene.F.p.value[, coef])
    else i <- order(fit$gene.F.p.value[, coef])[rank]
    geneid <- paste(fit$gene.genes[i, genecolname], collapse = ".")
  }
  else {
    geneid <- as.character(geneid)
    i <- which(fit$gene.genes[, genecolname] == geneid)[1]
    if (!length(i)) 
      stop(paste("geneid", geneid, "not found"))
  }
  j <- fit$gene.firstexon[i]:fit$gene.lastexon[i]
  exoncolname <- fit$exoncolname
  strcol <- grepl("strand", colnames(fit$gene.genes), ignore.case = TRUE)
  if (any(strcol)) 
    geneid <- paste0(geneid, " (", as.character(fit$gene.genes[i, 
                                                               strcol])[1], ")")
  if (is.null(exoncolname)) {
    plot(fit$coefficients[j, coef], xlab = "", ylab = "logFC (this junction vs average-rest)", 
         main = geneid, type = "b")
  }
  else {
    exon.id <- fit$genes[j, exoncolname]
    
    total.exon.plot <- diffSout[match(exon.id, diffSout$ExonID),]
    total.exon.ids <- total.exon.plot$ExonID
    total.exon.plot$FDR[which(is.na(total.exon.plot$FDR))] = 1
    total.exon.plot$logFC[which(is.na(total.exon.plot$logFC))] = 0
    total.exon.plot$mean.rawcount[which(is.na(total.exon.plot$mean.rawcount))] = 0
  }
  
  # transform 0 FDR into 1e-322
  total.exon.plot$FDR[which(total.exon.plot$FDR==0)] = 1.0e-322
  
  # Get FDRs of junctions in geneid
  fdr <- total.exon.plot$FDR
  sig <- fdr < FDR
  
  
  # Get mean read count of junctions in geneID
  tumor.withcounts.mean <- paste(total.exon.plot$tumor.samples.withcounts, "(", round(total.exon.plot$mean.rawcount, digits = 2) , ")")
  
  # Get sig mean values
  mean.sig <- total.exon.plot$mean.rawcount >= 10
  NOmean.sig <- total.exon.plot$mean.rawcount < 10
  
  # Get LogFC of junctions in geneid
  logFCs <- total.exon.plot$logFC
  pos.logFC <- logFCs > 2
  neg.logFC <- logFCs < -2
  
  sig.and.NOmean <- sig & NOmean.sig
  sig.and.mean <- sig & mean.sig
  
  sig.and.neg <- sig & neg.logFC
  sig.and.pos <- sig & pos.logFC
  
  sigmean.and.pos <- sig.and.mean & pos.logFC
  #sigmean.and.neg <- sig.and.mean & neg.logFC <- not necessary for negative logFC
  sigmean.and.neg <- sig.and.neg
  
  # Get absolute LogFC of junctions in geneid
  logFCs.ebayes <- total.exon.plot$logFC.ebayes
  sig.logFC.e <- logFCs.ebayes < -2 | logFCs.ebayes > 2
  
  color = rep("#000000", length(total.exon.ids))
  color[which(sigmean.and.neg=="TRUE")] = "#005BA2"
  color[which(sigmean.and.pos=="TRUE")] = "#D44F4F"
  color[which(sigmean.and.pos=="TRUE" & sig.logFC.e=="FALSE")] = "#D3D3D3"
  color[which(sigmean.and.pos=="TRUE" & sig.logFC.e=="FALSE")] = "#D3D3D3"
  color[which(sigmean.and.neg=="TRUE" & sig.logFC.e=="FALSE")] = "#D3D3D3"
  color[which(sigmean.and.neg=="TRUE" & sig.logFC.e=="FALSE")] = "#D3D3D3"
  
  total.exon.plot$color = color
  
  sigmean.and.pos <- sigmean.and.pos & sig.logFC.e
  sigmean.and.neg <- sigmean.and.neg & sig.logFC.e
  
  
  # Add size of bubble
  cex = rep(1, length(total.exon.ids))
  
  if (any(sigmean.and.pos)) {
    
    cex[which(sigmean.and.pos =="TRUE")] <- 3
  }
  
  
  if (any(sigmean.and.neg)) {
    cex[which(sigmean.and.neg =="TRUE")] <- 3
  }
  
  total.exon.plot$cex <- cex
  
  # Add information about group
  sig.junct <- total.exon.plot$ExonID[which(total.exon.plot$color!="#000000")]
  total.exon.plot$GrouplogFC <- NA
  total.exon.plot$GrouplogFC[which(total.exon.plot$color!="#000000")] <- SigTopTable$group[match(sig.junct, SigTopTable$ExonID)]
  total.exon.plot$GrouplogFC[which(is.na(total.exon.plot$GrouplogFC))] <- "no significant"
  
  # change colnames for compatibility with tooltip
  colnames(total.exon.plot)[which(colnames(total.exon.plot)=="tumor.samples.withcounts")] <- "tumor_samples_withcounts"
  colnames(total.exon.plot)[which(colnames(total.exon.plot)=="mean.rawcount")] <- "mean_rawcount"
  colnames(total.exon.plot)[which(colnames(total.exon.plot)=="logFC.ebayes")] <- "logFC_absolute"
  
  highchart()%>%
    hc_add_series(total.exon.plot$logFC, type = "line", showInLegend = F, color = "grey")%>%
    hc_add_series(data= total.exon.plot, hcaes(x=ExonID, y=logFC, color=color, size = cex), 
                  type = "scatter",showInLegend = F, maxSize = "5%")%>%
    # # here you add to your plot
    # hc_add_series(rep(0, length(total.exon.plot$logFC)), yAxis=0, color="red", dashStyle = "DashDot")%>%
    hc_xAxis(title = list(text = paste("junctions in", geneid)),categories = total.exon.ids)%>%
    hc_yAxis_multiples(
      list(title = list(text = "logFC (this junction vs average-rest)"), lineWidth = 3,
           minorGridLineWidth = 0,
           gridLineWidth = 0,
           # plotLines = list(
           #   list(value = 0, color = "red")
           # ),
           
           plotBands = list(
             list(from = -2, to = 0, color = "lightgrey"),
             list(from = 0, to = 2, color = "lightgrey")
           ),
           plotLines = list(
             list(value = 0, color = "black", width=3)
           )
           
           
      )
    )%>%
    hc_tooltip(pointFormat = "junction: {point.ExonID} <br> logFC diffSplice: {point.y} <br> logFC absolute: {point.logFC_absolute} <br> group: {point.GrouplogFC} <br> mean raw counts: {point.mean_rawcount} <br> number of samples with counts: {point.tumor_samples_withcounts} ",
               crosshairs=TRUE)
  
}



plotSplice.mod2 <- function (fit, coef = ncol(fit), geneid = NULL, genecolname = NULL, 
                            diffSout = NULL, SigTopTable=NULL, rank = 1L, FDR = 0.01) 
{
  if (is.null(genecolname)) 
    genecolname <- fit$genecolname
  else genecolname <- as.character(genecolname)
  if (is.null(geneid)) {
    if (rank == 1L) 
      i <- which.min(fit$gene.F.p.value[, coef])
    else i <- order(fit$gene.F.p.value[, coef])[rank]
    geneid <- paste(fit$gene.genes[i, genecolname], collapse = ".")
  }
  else {
    geneid <- as.character(geneid)
    i <- which(fit$gene.genes[, genecolname] == geneid)[1]
    if (!length(i)) 
      stop(paste("geneid", geneid, "not found"))
  }
  j <- fit$gene.firstjunction[i]:fit$gene.lastjunction[i]
  junctioncolname <- fit$junctioncolname
  strcol <- grepl("strand", colnames(fit$gene.genes), ignore.case = TRUE)
  if (any(strcol)) 
    geneid <- paste0(geneid, " (", as.character(fit$gene.genes[i, 
                                                               strcol])[1], ")")
  if (is.null(junctioncolname)) {
    plot(fit$coefficients[j, coef], xlab = "", ylab = "logFC (this junction vs average-rest)", 
         main = geneid, type = "b")
  }
  else {
    junction.id <- fit$genes[j, junctioncolname]
    
    total.junction.plot <- diffSout[match(junction.id, diffSout$junctionID),]
    total.junction.ids <- total.junction.plot$junctionID
    total.junction.plot$FDR[which(is.na(total.junction.plot$FDR))] = 1
    total.junction.plot$logFC[which(is.na(total.junction.plot$logFC))] = 0
    total.junction.plot$mean.rawcount[which(is.na(total.junction.plot$mean.rawcount))] = 0
  }
  
  # transform 0 FDR into 1e-322
  total.junction.plot$FDR[which(total.junction.plot$FDR==0)] = 1.0e-322
  
  # Get FDRs of junctions in geneid
  fdr <- total.junction.plot$FDR
  sig <- fdr < FDR
  
  
  # Get mean read count of junctions in geneID
  tumor.withcounts.mean <- paste(total.junction.plot$tumor.samples.withcounts, "(", round(total.junction.plot$mean.rawcount, digits = 2) , ")")
  
  # Get sig mean values
  mean.sig <- total.junction.plot$mean.rawcount >= 10
  NOmean.sig <- total.junction.plot$mean.rawcount < 10
  
  # Get LogFC of junctions in geneid
  logFCs <- total.junction.plot$logFC
  pos.logFC <- logFCs > 2
  neg.logFC <- logFCs < -2
  
  sig.and.NOmean <- sig & NOmean.sig
  sig.and.mean <- sig & mean.sig
  
  sig.and.neg <- sig & neg.logFC
  sig.and.pos <- sig & pos.logFC
  
  sigmean.and.pos <- sig.and.mean & pos.logFC
  #sigmean.and.neg <- sig.and.mean & neg.logFC <- not necessary for negative logFC
  sigmean.and.neg <- sig.and.neg
  
  # Get absolute LogFC of junctions in geneid
  logFCs.ebayes <- total.junction.plot$logFC.ebayes
  sig.logFC.e <- logFCs.ebayes < -2 | logFCs.ebayes > 2
  
  color = rep("#000000", length(total.junction.ids))
  color[which(sigmean.and.neg=="TRUE")] = "#005BA2"
  color[which(sigmean.and.pos=="TRUE")] = "#D44F4F"
  color[which(sigmean.and.pos=="TRUE" & sig.logFC.e=="FALSE")] = "#D3D3D3"
  color[which(sigmean.and.pos=="TRUE" & sig.logFC.e=="FALSE")] = "#D3D3D3"
  color[which(sigmean.and.neg=="TRUE" & sig.logFC.e=="FALSE")] = "#D3D3D3"
  color[which(sigmean.and.neg=="TRUE" & sig.logFC.e=="FALSE")] = "#D3D3D3"
  
  total.junction.plot$color = color
  
  sigmean.and.pos <- sigmean.and.pos & sig.logFC.e
  sigmean.and.neg <- sigmean.and.neg & sig.logFC.e
  
  
  # Add size of bubble
  cex = rep(1, length(total.junction.ids))
  
  if (any(sigmean.and.pos)) {
    
    cex[which(sigmean.and.pos =="TRUE")] <- 3
  }
  
  
  if (any(sigmean.and.neg)) {
    cex[which(sigmean.and.neg =="TRUE")] <- 3
  }
  
  total.junction.plot$cex <- cex
  
  # Add information about group
  sig.junct <- total.junction.plot$junctionID[which(total.junction.plot$color!="#000000")]
  total.junction.plot$GrouplogFC <- NA
  total.junction.plot$GrouplogFC[which(total.junction.plot$color!="#000000")] <- SigTopTable$group[match(sig.junct, SigTopTable$junctionID)]
  total.junction.plot$GrouplogFC[which(is.na(total.junction.plot$GrouplogFC))] <- "no significant"
  
  # change colnames for compatibility with tooltip
  colnames(total.junction.plot)[which(colnames(total.junction.plot)=="tumor.samples.withcounts")] <- "tumor_samples_withcounts"
  colnames(total.junction.plot)[which(colnames(total.junction.plot)=="mean.rawcount")] <- "mean_rawcount"
  colnames(total.junction.plot)[which(colnames(total.junction.plot)=="logFC.ebayes")] <- "logFC_absolute"
  
  highchart()%>%
    hc_add_series(total.junction.plot$logFC, type = "line", showInLegend = F, color = "grey")%>%
    hc_add_series(data= total.junction.plot, hcaes(x=junctionID, y=logFC, color=color, size = cex), 
                  type = "scatter",showInLegend = F, maxSize = "5%")%>%
    # # here you add to your plot
    # hc_add_series(rep(0, length(total.junction.plot$logFC)), yAxis=0, color="red", dashStyle = "DashDot")%>%
    hc_xAxis(title = list(text = paste("junctions in", geneid)),categories = total.junction.ids)%>%
    hc_yAxis_multiples(
      list(title = list(text = "logFC (this junction vs average-rest)"), lineWidth = 3,
           minorGridLineWidth = 0,
           gridLineWidth = 0,
           # plotLines = list(
           #   list(value = 0, color = "red")
           # ),
           
           plotBands = list(
             list(from = -2, to = 0, color = "lightgrey"),
             list(from = 0, to = 2, color = "lightgrey")
           ),
           plotLines = list(
             list(value = 0, color = "black", width=3)
           )
           
           
      )
    )%>%
    hc_tooltip(pointFormat = "junction: {point.junctionID} <br> logFC diffSplice: {point.y} <br> logFC absolute: {point.logFC_absolute} <br> group: {point.GrouplogFC} <br> mean raw counts: {point.mean_rawcount} <br> number of samples with counts: {point.tumor_samples_withcounts} ",
               crosshairs=TRUE)
  
}


plotSplice.mod2 <-   function (analize.out,
                               geneID = NULL,
                               gtf = NULL,
                               target.junction = NULL,
                               genecolname = NULL,
                               rank = 1L,
                               logFC = 1,
                               meanExp = 10,
                               FDR = 0.05)
{
  junctionID <- NULL
  fit = analize.out$ex.norm
  coef = ncol(fit)
  diffSout = analize.out$dje.out
  SigTopTable = analize.out$dje.sig
  geneid = analize.out$dje.out$GeneID[match(geneID, analize.out$dje.out$GeneID)]
  if (is.null(genecolname))
    genecolname <- fit$genecolname
  else
    genecolname <- as.character(genecolname)
  if (is.null(geneid)) {
    if (rank == 1L)
      i <- which.min(fit$gene.F.p.value[, coef])
    else
      i <- order(fit$gene.F.p.value[, coef])[rank]
    geneid <- paste(fit$gene.genes[i, genecolname], collapse = ".")
  }
  else {
    geneid <- as.character(geneid)
    i <- which(fit$gene.genes[, genecolname] == geneid)[1]
    if (!length(i))
      stop(paste("geneid", geneid, "not found"))
  }
  j <- fit$gene.firstjunction[i]:fit$gene.lastjunction[i]
  junctioncolname <- fit$junctioncolname
  strcol <-
    grepl("strand", colnames(fit$gene.genes), ignore.case = TRUE)
  if (any(strcol))
    geneid <- paste0(geneid, " (", as.character(fit$gene.genes[i,
                                                               strcol])[1], ")")
  if (is.null(junctioncolname)) {
    plot(
      fit$coefficients[j, coef],
      xlab = "",
      ylab = "logFC (this junction vs average-rest)",
      main = geneid,
      type = "b"
    )
  }
  else {
    junction.id <- fit$genes[j, junctioncolname]
    tjunct <- diffSout[match(junction.id, diffSout$junctionID), ]
    total.junction.ids <- tjunct$junctionID
    tjunct$FDR[which(is.na(tjunct$FDR))] = 1
    tjunct$logFC[which(is.na(tjunct$logFC))] = 0
    tjunct$meanExp.group2[which(is.na(tjunct$meanExp.group2))] = 0
    tjunct$meanExp.group2 <-
      as.numeric(as.character(tjunct$meanExp.group2))
  }
  tjunct$FDR[which(tjunct$FDR == 0)] = 1.0e-322
  fdr <- tjunct$FDR
  sig <- fdr < FDR
  tumor.withcounts.mean <-
    paste(tjunct$group2WithCounts,
          "(",
          round(tjunct$meanExp.group2, digits = 2) ,
          ")")
  mean.sig <- tjunct$meanExp.group2 >= meanExp
  NOmean.sig <- tjunct$meanExp.group2 < meanExp
  logFCs <- tjunct$logFC
  pos.logFC <- logFCs > logFC
  neg.logFC <- logFCs < -logFC
  sig.and.NOmean <- sig & NOmean.sig
  sig.and.mean <- sig & mean.sig
  sig.and.neg <- sig & neg.logFC
  sig.and.pos <- sig & pos.logFC
  sigmean.and.pos <- sig.and.mean & pos.logFC
  sigmean.and.neg <- sig.and.neg
  logFCs.ebayes <- tjunct$logFC.ebayes
  sig.logFC.e <- logFCs.ebayes < -logFC | logFCs.ebayes > logFC
  color = rep("#000000", length(total.junction.ids))
  color[which(sigmean.and.neg == "TRUE")] = "#005BA2"
  color[which(sigmean.and.pos == "TRUE")] = "#D44F4F"
  color[which(sigmean.and.pos == "TRUE" &
                sig.logFC.e == "FALSE")] = "#D3D3D3"
  color[which(sigmean.and.pos == "TRUE" &
                sig.logFC.e == "FALSE")] = "#D3D3D3"
  color[which(sigmean.and.neg == "TRUE" &
                sig.logFC.e == "FALSE")] = "#D3D3D3"
  color[which(sigmean.and.neg == "TRUE" &
                sig.logFC.e == "FALSE")] = "#D3D3D3"
  tjunct$color = color
  sigmean.and.pos <- sigmean.and.pos & sig.logFC.e
  sigmean.and.neg <- sigmean.and.neg & sig.logFC.e
  cex = rep(1, length(total.junction.ids))
  if (any(sigmean.and.pos)) {
    cex[which(sigmean.and.pos == "TRUE")] <- 3
  }
  if (any(sigmean.and.neg)) {
    cex[which(sigmean.and.neg == "TRUE")] <- 3
  }
  tjunct$cex <- cex
  sig.junct <- tjunct$junctionID[which(tjunct$color != "#000000")]
  tjunct$GrouplogFC <- NA
  tjunct$GrouplogFC[which(tjunct$color != "#000000")] <-
    SigTopTable$group[match(sig.junct, SigTopTable$junctionID)]
  tjunct$GrouplogFC[which(is.na(tjunct$GrouplogFC))] <-
    "no significant"
  tjunct$group2WithCounts <-
    round(as.numeric(as.character(tjunct$group2WithCounts)), digits = 2)
  tjunct$meanExp.group2 <-
    round(as.numeric(as.character(tjunct$meanExp.group2)), digits = 2)
  tjunct$logFC.ebayes <-
    round(as.numeric(as.character(tjunct$logFC.ebayes)), digits = 2)
  colnames(tjunct)[which(colnames(tjunct) == "meanExp.group2")] <-
    "meanExp_group2"
  colnames(tjunct)[which(colnames(tjunct) == "logFC.ebayes")] <-
    "logFC_absolute"
  p <- highcharter::highchart() %>%
    highcharter::hc_add_series(
      tjunct$logFC,
      type = "line",
      showInLegend = F,
      color = "grey"
    ) %>%
    highcharter::hc_add_series(
      data = tjunct,
      highcharter::hcaes(
        x = junctionID,
        y = logFC,
        color = color,
        size = cex
      ),
      type = "scatter",
      showInLegend = F,
      maxSize = "5%"
    ) %>%
    highcharter::hc_xAxis(title = list(text = paste("junctions in", geneid)), categories = total.junction.ids) %>%
    highcharter::hc_yAxis_multiples(
      list(
        title = list(text = "logFC (diffSplice)"),
        lineWidth = 3,
        minorGridLineWidth = 0,
        gridLineWidth = 0,
        plotBands = list(
          list(
            from = -logFC,
            to = 0,
            color = "lightgrey"
          ),
          list(from = 0, to = logFC, color = "lightgrey")
        ),
        plotLines = list(list(
          value = 0,
          color = "black",
          width = 3
        ))
      )
    ) %>%
    highcharter::hc_tooltip(pointFormat = "junction: {point.junctionID} <br> logFC diffSplice: {point.y} <br> logFC absolute: {point.logFC_absolute} <br> FDR: {point.FDR} <br> group: {point.GrouplogFC} <br> mean raw counts: {point.meanExp_group2} <br> number of samples with counts: {point.group2WithCounts} ",
                            crosshairs = TRUE)
  
}



# modified version of buildElemes from shinyCyJS
buildElems.mod <- function (elems, type) 
{
  func <- paste0("build", type, "(")
  res <- list()
  params <- colnames(elems)
  for (i in 1:nrow(elems)) {
    command <- c()
    for (j in 1:length(params)) {
      if (class(elems[i,j]) =="character"){
        command[j] <- paste0(params[j], " = ", "'", elems[i,j], "'")
      }else{
        command[j] <- paste0(params[j], " = ", elems[i,j])
      }
    }
    command <- paste(command, collapse = ", ")
    command <- paste0(func, command, ")")
    res[[length(res) + 1]] <- eval(parse(text = command))
  }
  return(res)
}
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
